using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    public GameObject projectilePrefab;
    private bool shootDown;
    public float valorCronometro = 2f;
    public bool volverAPulsar = true;


    // Start is called before the first frame update
    void Start()
    {
        shootDown = false;
        volverAPulsar = true;
    }

    // Update is called once per frame
    void Update()
    {
        /*if (Input.GetButtonDown("Fire1"))
        {
           Instantiate(projectilePrefab, transform.position, Quaternion.identity);
        }*/

        shootSpace();
    }

    public void PointerDown()
    {
        shootDown = true;
    }

    public void PointerUp()
    {
        shootDown = false;
    }

    private void shootSpace()
    {
        Debug.Log("Se esta ejecutando");
        if (shootDown == true && volverAPulsar == true)
        {
            Debug.Log("Se esta ejecutando dentro del if");
            Instantiate(projectilePrefab, transform.position, Quaternion.identity);
            volverAPulsar = false;
            StartCoroutine(CadenciaDeDisparo());
        }

    }
    public IEnumerator CadenciaDeDisparo()
    {
        
            yield return new WaitForSeconds(1f);
            
            Debug.Log("Dispara");

            volverAPulsar = true;


    }
}
