using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VibrationManager : MonoBehaviour
{
  

    public static void Vibrate()
    {
        // Check if device supports vibration
        if (SystemInfo.supportsVibration)
        {
            // Trigger vibration
            Handheld.Vibrate();
        }
    }
}
